

![!](./background/integrations.png){ loading=lazy style="width:50%" align=right }

<!--|cog p(f'## {values.project.name} ') |-->
## SaaSLess 
<!--|end|-->


<!--|cog p(f'{values.project.description} ') |-->
A complete Software-As-A-Service project made using the FlaskLess project 
<!--|end|-->

{# include-markdown "../../README.md" 
   start="<!---|START introduction|--->"
   end="<!---|END introduction|--->"
#}


## Features

  * **AdminLTE3** theme using Bootstrap 4.
  * **Flask-Admin** - quickly create user interfaces (create, read, update, delete) for models.
  * **Flask-Security-Too** - role-based-access-control, two-factor-authentication and much more.
  * **Navigation based on YAML** - no need to write error-prone HTML for navigation.
  * **Serverless** - ready to deploy to AWS Lambda using [Serverless](https://serverless.com).


![./images/saasychefhero.png](./images/saasychefhero-small.png){ loading=lazy align=right }
